import {
    GetProductsForIngredient,
    GetRecipes
} from "./supporting-files/data-access";
import {
    Recipe} from "./supporting-files/models";
import {
    GetCheapestSupplier,
    Sorter,
    SumUnitsOfMeasure
} from "./supporting-files/helpers";
import {RunTest, ExpectedRecipeSummary} from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */
const GetCheapestRecipe = (recipe: Recipe) => {
    let cheapestRecipe = {
        cheapestCost: 0,
        nutrientsAtCheapestCost: {}
    };
    
    recipe.lineItems.forEach((item) => {
        const cheapestProducts = GetCheapestSupplier(GetProductsForIngredient(item.ingredient)); //Get products from the cheapest suppliers
        cheapestRecipe.cheapestCost += cheapestProducts.cost * item.unitOfMeasure.uomAmount; //Getting total cost
        cheapestProducts.nutrientFacts.forEach((fact) => {
            if (!(fact.nutrientName in cheapestRecipe.nutrientsAtCheapestCost)) { //Adds nutrient to nutrient facts if not added
                cheapestRecipe.nutrientsAtCheapestCost[fact.nutrientName] = fact;
                return;
            }
            //Get sum of nutrient amounts
            cheapestRecipe.nutrientsAtCheapestCost[fact.nutrientName].quantityAmount = SumUnitsOfMeasure(cheapestRecipe.nutrientsAtCheapestCost[fact.nutrientName].quantityAmount, fact.quantityAmount);
        });
    });
    
    cheapestRecipe.nutrientsAtCheapestCost = Sorter(cheapestRecipe.nutrientsAtCheapestCost); //Sorts nutrients
    return cheapestRecipe;
}
  
recipeData.forEach((recipe) => {
    recipeSummary[recipe.recipeName] = GetCheapestRecipe(recipe);
})

console.log(recipeSummary);


/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
