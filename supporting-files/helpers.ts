import {
    NutrientFact,
    Product,
    SupplierProduct,
    UnitOfMeasure,
    UoMName,
    UoMType
} from "./models";
import {GetBaseUoM, GetUnitsData, NutrientBaseUoM} from "./data-access";

/*
* Converts from one unit of measure to another requested unit of measure
*/
export function ConvertUnits(
    fromUoM: UnitOfMeasure,
    toUoMName: UoMName,
    toUoMType: UoMType
): UnitOfMeasure {
    if (fromUoM.uomName === toUoMName && fromUoM.uomType === toUoMType)
        return fromUoM;

    const conversionRate = GetUnitsData().find(
        (x) =>
            x.fromUnitName === fromUoM.uomName &&
            x.fromUnitType === fromUoM.uomType &&
            x.toUnitName === toUoMName &&
            x.toUnitType === toUoMType
    );

    if (!conversionRate)
        throw new Error(`Couldn't convert ${fromUoM.uomName} to ${toUoMName}`);

    return {
        uomAmount: fromUoM.uomAmount * conversionRate.conversionFactor,
        uomName: toUoMName,
        uomType: toUoMType
    };
}

/*
* Takes in two units of measure and returns the sum of both in the same base unit as A
*/
export function SumUnitsOfMeasure(
    uomA: UnitOfMeasure,
    uomB: UnitOfMeasure
): UnitOfMeasure {
    const convertedUomB = ConvertUnits(uomB, uomA.uomName, uomA.uomType);
    return {
        uomAmount: uomA.uomAmount + convertedUomB.uomAmount,
        uomName: uomA.uomName,
        uomType: uomA.uomType
    };
}

/*
* Calculates the cost price for a given supplier product,
* in the base unit of measure for that product
*/
export function GetCostPerBaseUnit(supplierProduct: SupplierProduct): number {
    const baseUnitOfMeasure = GetBaseUoM(
        supplierProduct.supplierProductUoM.uomType
    );

    const converted = ConvertUnits(
        supplierProduct.supplierProductUoM,
        baseUnitOfMeasure.uomName,
        baseUnitOfMeasure.uomType
    );

    return supplierProduct.supplierPrice / converted.uomAmount;
}

/*
* Takes in a nutrient fact and converts its measurements into the appropriate
* base unit of measure for nutrients
*/
export function GetNutrientFactInBaseUnits(fact: NutrientFact): NutrientFact {
    return {
        nutrientName: fact.nutrientName,
        quantityAmount: ConvertUnits(
            fact.quantityAmount,
            NutrientBaseUoM.uomName,
            NutrientBaseUoM.uomType
        ),
        quantityPer: ConvertUnits(
            fact.quantityPer,
            NutrientBaseUoM.uomName,
            NutrientBaseUoM.uomType
        )
    };
}

// Added sorting function for the nutrients

export function Sorter(item: any){
    const sorted = {};
    const keys = Object.keys(item).sort();
  
    keys.forEach((key, val) => {
      sorted[key] = item[key];
    });
  
    return sorted;
};


// Added function to get cheapest per unit ingredient with nutrient facts
// Expects array of Products and returns a sorted array of Products

export function GetCheapestSupplier(product: Product[]){
    return product
    .map((productIngredient: Product) => {
        // getting the supplierProduct with the cheapest cost per unit
        const cheapestSupplier: number[] = productIngredient.supplierProducts.map(
            (supplierProduct: SupplierProduct) => GetCostPerBaseUnit(supplierProduct)
        );
        // getting the nutrient facts
        const nutrientFacts: NutrientFact[] = productIngredient.nutrientFacts.map(
            nutrientFact => GetNutrientFactInBaseUnits(nutrientFact)
        );

        return {
            cost: Math.min.apply(Math, cheapestSupplier),
            nutrientFacts,
        };
    })
    .reduce((a, b) => (a.cost < b.cost ? a : b)); //sorting
}